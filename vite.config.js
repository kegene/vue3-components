import { resolve } from 'node:path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import AutoImport from 'unplugin-auto-import/vite'

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            '@': resolve(__dirname, './src'),
        },
    },
    plugins: [
        vue(),
        AutoImport({
            imports: [
                'vue',
                'vue-router',
                '@vueuse/core',
            ],
            dirs: ['./src/composables/**'],
            dts: false, // 沒使用 ts 可 false
        }),
        Components({
            resolvers: [
                IconsResolver({
                    enabledCollections: ['mdi', 'bi'],
                }),
            ],
            dts: false, // 沒使用 ts 可 false
        }),
        // https://github.com/unplugin/unplugin-icons#auto-importing
        Icons({
            autoInstall: true,
        }),
    ],
})
