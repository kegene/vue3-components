import UiTabs from './UiTabs.vue'
import UiTabPane from './UiTabPane.vue'

export { UiTabPane }
export default UiTabs
