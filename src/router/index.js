import { createRouter, createWebHistory } from 'vue-router'

import Root from './root'

const modules = import.meta.glob('@/router/modules/*.js', { eager: true }) || []
const routes = [...Root]

for (const key in modules)
    routes.push(...modules[key].default)

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
    scrollBehavior: () => ({ left: 0, top: 0 }),
})

// router.beforeEach(async (to, from) => {
// todo check auth Token
// })

export default router
