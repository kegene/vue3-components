export default [
    {
        path: '/demo',
        name: 'Demo',
        component: () => import(/* webpackChunkName: "Demo" */ '@/views/demo/Demo.vue'),
    },
]
