import Home from '@/views/Home.vue'

const routes = [{
    path: '/',
    name: 'root',
    redirect: '/',
    component: () => import(/* webpackChunkName: "BaseLayout" */ '@/layouts/BaseLayout.vue'),
    children: [
        {
            path: '',
            name: 'home',
            component: Home,
        },
        {
            path: 'error404',
            name: 'error404',
            component: () => import('@/views/error/Error404.vue'),
        },
        { path: '/:pathMatch(.*)*', name: 'NotFound', component: () => import('@/views/error/Error404.vue') },
    ],
}]

export default routes
